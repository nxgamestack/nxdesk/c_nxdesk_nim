{.deadCodeElim: on.}
when defined(windows):
  const
    libnxdesk* = "nxdesk.dll"
elif defined(macosx):
  const
    libnxdesk* = "libnxdesk.dylib"
else:
  const
    libnxdesk* = "libnxdesk.so"

{.push callConv:cdecl, header: "nxdesk/api/prefix_desk.h", dynlib: libnxdesk.}

proc c_nxdesk_new_desk(): cint
    {.importc: "nxdesk_new_desk".}
proc c_nxdesk_cleanup_all()
    {.importc: "nxdesk_cleanup_all".}

proc c_nxdesk_has_board(desk_id: cint; board_name: cstring):cint
    {.importc: "nxdesk_has_board".}
proc c_nxdesk_has_token(desk_id: cint; board_name: cstring; token_name: cstring):cint
    {.importc: "nxdesk_has_token", discardable.}
proc c_nxdesk_has_property(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring):cint
    {.importc: "nxdesk_has_property", discardable.}

proc c_nxdesk_touch_board(desk_id: cint; board_name: cstring):cint
    {.importc: "nxdesk_touch_board", discardable.}
proc c_nxdesk_touch_token(desk_id: cint; board_name: cstring; token_name: cstring):cint
    {.importc: "nxdesk_touch_token", discardable.}

proc c_nxdesk_get_kind(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring):cint
    {.importc: "nxdesk_get_kind", discardable.}
proc c_nxdesk_remove_property(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring)
    {.importc: "nxdesk_remove_property".}

proc c_nxdesk_get_int64(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring):clonglong
    {.importc: "nxdesk_get_int64", discardable.}
proc c_nxdesk_set_int64(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring; value: clonglong):cint
    {.importc: "nxdesk_set_int64", discardable.}

proc c_nxdesk_get_float64(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring):cdouble
    {.importc: "nxdesk_get_float64", discardable.}
proc c_nxdesk_set_float64(desk_id: cint; board_name: cstring; token_name: cstring; property_name: cstring; value: cdouble):cint
    {.importc: "nxdesk_set_float64", discardable.}

#proc c_nxdesk_a():
#    {.importc: "".}


{.pop.}

proc nxdesk_new_desk*():int=
    return c_nxdesk_new_desk()
proc nxdesk_cleanup_all*(desk_id: int)=
    c_nxdesk_cleanup_all()

proc nxdesk_has_board*(desk_id: int; board_name: string):int {.discardable.} =
    return c_nxdesk_has_board(cint(desk_id), board_name)

proc nxdesk_has_token*(desk_id: int; board_name: string; token_name: string):int {.discardable.} =
    return c_nxdesk_has_token(cint(desk_id), board_name, token_name)
proc nxdesk_has_property*(desk_id: int; board_name: string; token_name: string; property_name: string):int {.discardable.} =
    return c_nxdesk_has_property(cint(desk_id), board_name, token_name, property_name)

proc nxdesk_touch_board*(desk_id: int; board_name: string):int {.discardable.} =
    return c_nxdesk_touch_board(cint(desk_id), board_name)
proc nxdesk_touch_token*(desk_id: int; board_name: string; token_name: string):int {.discardable.} =
    return c_nxdesk_touch_token(cint(desk_id), board_name, token_name)

proc nxdesk_get_kind*(desk_id: int; board_name: string; token_name: string; property_name: string):int {.discardable.} =
    return c_nxdesk_get_kind(cint(desk_id), board_name, token_name, property_name)
proc nxdesk_remove_property*(desk_id: int; board_name: string; token_name: string; property_name: string):int {.discardable.} =
    c_nxdesk_remove_property(cint(desk_id), board_name, token_name, property_name)

proc nxdesk_get_int64*(desk_id: int; board_name: string; token_name: string; property_name: string):int64 =
    return c_nxdesk_get_int64(cint(desk_id), board_name, token_name, property_name)
proc nxdesk_set_int64*(desk_id: int; board_name: string; token_name: string; property_name: string; value: int64):int {.discardable.} =
    return c_nxdesk_set_int64(cint(desk_id), board_name, token_name, property_name, clonglong(value))

proc nxdesk_get_float64*(desk_id: int; board_name: string; token_name: string; property_name: string):float64 =
    return c_nxdesk_get_float64(cint(desk_id), board_name, token_name, property_name)
proc nxdesk_set_float64*(desk_id: int; board_name: string; token_name: string; property_name: string; value: float64):int {.discardable.} =
    return c_nxdesk_set_float64(cint(desk_id), board_name, token_name, property_name, cdouble(value))
