# Package

version       = "0.0.2"
author        = "nexustix"
description   = "Library for nxdesk datastructures"
license       = "BSD2"
srcDir        = "src"


# Dependencies

requires "nim >= 1.0.0"